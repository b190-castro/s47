/* Using DOM

	before, we are using the following codes in terms of selecting elements inside the html file

*/
// document refers to the whole webpage and the "getElementById","getElementByClassName" and "getElementByTagName" selects the element with the same id as its arguments
/*document.getElementById('text-first-name');
document.getElementByClassName('text-last-name');
document.getElementByTagName('input');*/

// querySelector replaces the three "getElement" selectors and makes use of CSS format in terms of selectig the elements inside the htmal as its arguements (# - id, . - class, tagName - tag)
document.querySelector('#txt-first-name');

// Event Listeners
// events are all interactions of the user to our webpage; such examples are clicking, hovering, reloading, keypressing/keyup(typing);
const txtFirstName = document.querySelector('#txt-first-name');
const spanFirstName = document.querySelector('#span-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanLastName = document.querySelector('#span-last-name');
// to perform an action when an event triggers, first you have to listen to it

/*
	the function used is the addEventListener - allows a block of codes to listen to an event for them to be executed
		addEventListener - takes two arguements: 
		1 - a string that identifies the event to whch the codes will listen;
		2 - a function that the listener will execute once the specified event is triggered
*/
txtFirstName.addEventListener('keyup', (event) => {
	spanFirstName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener('keyup', (event) => {
	spanLastName.innerHTML = txtLastName.value;
})

// https://www.w3schools.com/jsref/dom_obj_event.asp
// https://developer.mozilla.org/en-US/docs/Web/Events

// multiple listeners can also be assigned to same event; in the same way, same listeners can listen to different events
// txtFirstName.addEventListener('keypress', (event) => {
/*txtFirstName.addEventListener('keyup', (event) => {
	// event - contains the information the triggered event passed from the first argument
	// event.target - contains the element where the event happened
	// event.target.value - gets the value of the input object
	console.log(event.target);
	console.log(event.target.value);
});*/